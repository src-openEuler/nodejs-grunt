%global enable_tests 1
Name:                nodejs-grunt
Version:             1.0.1
Release:             6
Summary:             Grunt is a JavaScript library used for automation and running tasks
License:             MIT
URL:                 https://github.com/gruntjs/grunt
Source0:             https://github.com/gruntjs/grunt/archive/v%{version}/grunt-%{version}.tar.gz
Patch0:              CVE-2020-7729-pre.patch
Patch1:              CVE-2020-7729.patch
# https://github.com/gruntjs/grunt/commit/aad3d45
Patch2:              CVE-2022-0436.patch
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       npm(coffee-script) npm(dateformat) npm(eventemitter2) npm(exit)
BuildRequires:       npm(findup-sync) npm(glob) npm(grunt-cli) npm(grunt-known-options)
BuildRequires:       npm(grunt-legacy-log) npm(grunt-legacy-util) npm(iconv-lite) npm(js-yaml)
BuildRequires:       npm(minimatch) npm(nopt) npm(path-is-absolute) npm(rimraf) npm(difflet)
BuildRequires:       npm(grunt-contrib-nodeunit) npm(semver) npm(shelljs)
BuildRequires:       npm(temporary) npm(through2)
%endif
%description
Grunt is the JavaScript task runner. Why use a task runner? In one word:
automation. The less work you have to do when performing repetitive tasks
like minification, compilation, unit testing, linting, etc, the easier
your job becomes. After you've configured it, a task runner can do most
of that mundane work for you with basically zero effort.

%prep
%autosetup -n grunt-%{version} -p1
%nodejs_fixdep coffee-script '^1.3'
%nodejs_fixdep dateformat '*'
%nodejs_fixdep eventemitter2 '^6.4.5'
%nodejs_fixdep findup-sync '~0.3'
%nodejs_fixdep glob '~6.0.3'
%nodejs_fixdep minimatch '^3.0.0'
%nodejs_fixdep nopt '^3.0.6'
%nodejs_fixdep rimraf '^2.0'
%nodejs_fixdep js-yaml '^3.5.0'

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/grunt
cp -pr package.json internal-tasks/ lib/ \
    %{buildroot}%{nodejs_sitelib}/grunt
%nodejs_symlink_deps
%if 0%{?enable_tests}

%check
%nodejs_symlink_deps --check
grunt nodeunit:all
%endif

%files
%doc AUTHORS CHANGELOG CONTRIBUTING.md README.md
%license LICENSE
%{nodejs_sitelib}/grunt

%changelog
* Sat Sep 02 2023 Ge Wang <wang__ge@126.com> - 1.0.1-6
- Modify minimatch version to fix install problem

* Fri Jul 01 2022 baizhonggui <baizhonggui@h-partners.com> - 1.0.1-5
- Modify eventemitter version to 6.4.5 to compat latest eventemitter

* Mon May 09 2022 wangkai <wangkai385@h-partners.com> - 1.0.1-4
- Remove BuildRequires npm(grunt-contrib-watch)

* Thu Apr 21 2022 wangkai <wangkai385@h-partners.com> - 1.0.1-3
- Fix CVE-2022-0436

* Wed Feb 23 2022 yaoxin <yaoxin30@huawei.com> - 1.0.1-2
- Fix CVE-2020-7729

* Thu Aug 20 2020 Anan Fu <fuanan3@huawei.com> - 1.0.1-1
- package init
